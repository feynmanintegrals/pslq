/* 5 Nov 2006
 *   Oleg Veretin
 * 
 * This file strats reads data from file 'pslq.in'
 * and strts PSLQ2
 *
 */

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include "PSLQ2.h"

using namespace std;
using namespace cln;




int main(int argc, char **argv)
{
    int n, count, res;

    if (argc <= 1) {
        cout << "Usage: ./dopslq <FILENAME>" << endl << "(for example, use examples/pslq1.inp)" << endl;
        return 0;
    }

    string str;
    ifstream in_pslq;
    long dec = 200;
    float_format_t f = float_format(dec);

    vector<string> const_name;
    cl_R x;
    vector<cl_R> xin,xout;

    /*                               */
    /*  read PSLQ data from the file */
    /*                               */


    cout << "read form '" << argv[1] << "'..." << endl;

    in_pslq.open( argv[1] );

    if (!in_pslq) {
        cout << "PNAME: cant open file: <"<< argv[1]  << ">\n";
        cout << "Exit.\n" << flush;
        return 0;
    }

    count = 0;
    while ( !in_pslq.eof() ){
        in_pslq >> str;
        if ( str.find("=",0) != string::npos ){  // looking for '=' in the string
            count++;
            const_name.push_back(str);
            in_pslq >> x;
            xin.push_back(x);
        }
        if ( ( str.find("EOF",0) != string::npos ) || ( str.find("eof",0) != string::npos ) ){
            cout << " ... end of file reached.\n";
            break;
        }
    }
    cout << count << " numbers are read in." << endl << endl;
    in_pslq.close();


    cout << const_name.size() << endl;
    cout << xin.size() << endl;

    res = PSLQ2( xout, xin, dec, -dec+50, -15 );

    /* make output */
    if ( res == 1 ){
        cout << "It works!" << endl;
        if ( xout[0] != 0 ){
            cout
                << const_name[0].substr(0, const_name[0].find("=",0))  // removes '=' from string
                << " = ( "
                << endl;
            for (int i=1; i<xin.size(); i++){
                if ( xout[i] != 0 ){
                    cout
                        << "+ "
                        << const_name[i].substr(0, const_name[i].find("=",0))
                        << " * ( "
                        << -xout[i]/xout[0]
                        << " )"
                        << endl;
                }
            }
            cout << " )" << endl;
        }else{
            for (int i=0; i<xin.size(); i++){
                if ( xout[i] != 0 ){
                    cout << const_name[i] << ">    " << xout[i] << endl;
                }
            }
        }
    }

}
