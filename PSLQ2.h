/* 5 Nov 2006
 *   Oleg Veretin
 */

#ifndef PSLQ2_H
#define PSLQ2_H
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <string.h>

#include <cln/cln.h>

using namespace std;
using namespace cln;


/**************************************************************************
 *
 *  This is a 2-level PSLQ algorithm.                                     
 *
 *  integer relation finding algorithm is decribed in e.g.                
 *  see                                                                 
 *          D.Bailey, M.Borwein and  R.Girgensohn                        
 *        "Experimental Evaluation of Euler Sums",                       
 *           RNR Technical Report RNR-93-014.                            
 *                                                                       
 *
 *  Here we can use PSLQ in 3 different modes: 
 *    MP -- multiprecision (this is original algorithm)
 *    IP -- using 2-level algo with intermediate precision numbers
 *    DP -- using 2-level algo with double precision
 *
 *   Mode is defined by preprocessor variable PSLQ_MODE to be set MP, IP or DP.
 *
 *  DP mode is the fastest (recomended), but by some large problems it may be
 *  not effective. Try then IP.
 *  MP mode is alwais safe but very slow.
 * 
 *                                                                      
 *  Usage:  res = PSLQ(xout, xin, pslq_ndp, pslq_nep, pslq_nep1)
 *
 *  arguments:                                                          
 *       xout        --- output vector of <cl_R>                             
 *       xin         --- input vector of <cl_R>                             
 *       pslq_ndp    --- nuber of decimals of precision  <long>          
 *       pslq_nep    --- log10 of detection threshold and/or <long>          
 *                  machine epsilon                                     
 *                  tipically take    nep = - ndp + 15
 *                                 or nep = - ndp + 25 for large problems
 *                                                                      
 *       pslq_nep1   --- log10 of confidence <long>
 *                       (this cuts "solutions" with large ymin/ymax)
 *               Actually the "solution" will be printed if ymin/ymax < 10^(pslq_nep1)                            
 *               Usually  pslq_nep1 = -15 is a good choice.
 *               To recover original PSLQ set pslq_nep to zero
 *                                                                      
 *  returns:                                                             
 *         -1     error                                                 
 *          1     relation detected                                     
 *                    (output in vector  xout)                          
 *          2     precision exhausted                                   
 *          3     bound norm exceeds    PSLQ_BOUND                      
 *          4     too many iterations   PSLQ_ITERATIONS                 
 *                                                                      
 **************************************************************************/

/* here is the main function to call PSLQ2 */
int PSLQ2 (
            vector<cl_R>& xout, 
            const vector<cl_R>&  vec, 
            const long pslq_ndp, 
            const long pslq_nep, 
            const long pslq_nep1
	    );

#endif
