
:Begin:
:Function:      runpslq
:Pattern:       runPSLQ[x_List,p1_Integer,p2_Integer,p3_Integer]
:Arguments:     {x,p1,p2,p3}
:ArgumentTypes: {Manual}
:ReturnType:    Manual
:End:



:Evaluate:      Print["PSLQ ready!"]
:Evaluate:	PSLQ[yy_,xx_,p1_,p2_,p3_]:=Module[{x,temp},x=Prepend[ToString[SetPrecision[N[##,p1],p1]]&/@xx,ToString[yy]];temp=runPSLQ[x,p1,p2,p3];If[temp=={},Return[yy]];temp=Round/@temp;If[temp[[1]]===0,Print["Dependant constants;"];Return[False]];Drop[-temp/temp[[1]],1].xx]

:Evaluate:      PSLQ::usage = "PSLQ[list,p1,p2,p3]"



#include "mathlink.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include "PSLQ2.h"

using namespace std;
using namespace cln;

vector<string> const_name;
cl_R x;




void runpslq()
{
    cl_R x;
    vector<cl_R> xin;
    vector<cl_R> xout;
    int xinl;
    int p1,p2,p3;
    const char *s;
    MLGetFunction(stdlink,&s,&xinl);
    double res[200];
    for ( int i = 0; i<xinl; i++ ){
        MLGetString(stdlink,&s);
        char* str=(char *)s;
        char *buf=(char *)malloc(strlen(str)+1);
        char *str2=buf;

        for(;*str!='\0';str++){
            if( (*str=='\\')&&(str[1]=='0')&&(str[2]=='1')&&(str[3]=='2') )
                str+=3;
            else
                *str2++=*str;
        }
        *str2='\0';


        x=(cl_R)buf;
        free(buf);
        //MLReleaseString(stdlink,s);

        xin.push_back (x);      //      make xin - input vector
    }

    MLGetInteger(stdlink,&p1);
    MLGetInteger(stdlink,&p2);
    MLGetInteger(stdlink,&p3);


    int res_code = PSLQ2(xout,xin,p1,p2,p3);

    if (res_code != 1) {
        MLPutRealList(stdlink,res,0);
        return;
    }

    for ( int i = 0; i<xinl; i++ ){
        res[i]=double_approx(xout[i]);
    }

    MLPutRealList(stdlink,res,xinl);

    return;
}


int main(int argc,char *argv[])
{
    return MLMain(argc, argv);
}

