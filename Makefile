UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
MATHEXE=/Applications/Mathematica.app/Contents/MacOS/MathKernel
else
MATHEXE=math
endif

SHELL=/bin/bash
VERSION := $(shell echo -e $$ VersionNumber | tr -d " " | $(MATHEXE) -noprompt | tr -d ".\"\n" )
MBASE := $(shell echo -e $$ InstallationDirectory | tr -d " " | $(MATHEXE) -noprompt | tr -d "\"\n")
SYSID :=  $(shell echo -e $$ SystemID | tr -d " " | $(MATHEXE) -noprompt | tr -d "\"\n" )
MLINKDIR = ${MBASE}/SystemFiles/Links/MathLink/DeveloperKit
ifeq ($(SYSID), Linux)
	BIT := 32
endif
ifeq ($(SYSID), Linux-x86-64)
	BIT := 64
endif

ifeq ($(shell echo "$(VERSION)>=10" | bc), 1)
EXTRALIBS= -luuid -ldl
MLIBVERSION=4
else
MLIBVERSION=3
endif

INCDIR = ${MLINKDIR}/${SYSID}/CompilerAdditions
MLIB = ${INCDIR}/libML${BIT}i${MLIBVERSION}.a
MPREP = ${INCDIR}/mprep

ifeq ($(UNAME_S),Darwin)
MLIBS=-L${INCDIR} -lMLi${MLIBVERSION} -stdlib=libc++ -framework Foundation -lz -lm -mmacosx-version-min=10.9
LIBS=-stdlib=libc++ -framework Foundation -lz -lm -mmacosx-version-min=10.9
else
MLIBS=${MLIB} ${EXTRALIBS} -Wl,-Bstatic -Wl,-Bdynamic -pthread -lstdc++ -lrt -lz -lm
LIBS=-Wl,-Bstatic -Wl,-Bdynamic -pthread -lstdc++ -lrt -lz -lm
endif

LFLAGS = -lcln
CC = g++
CXX = gcc

CFLAGS = -O2 -I${INCDIR} -I/usr/local/include -DMLIBVERSION=${MLIBVERSION}

MPROGRAM=pslqface
DPROGRAM=dopslq

SRC = PSLQ2.h PSLQ2.cpp pslqface.tm

MOBJ = PSLQ2.o pslqface.o

DOBJ = PSLQ2.o dopslq.o

all: $(MPROGRAM) $(DPROGRAM)

$(DPROGRAM): $(DOBJ)
	$(CC) $(CFLAGS) -o $(DPROGRAM) $(DOBJ) ${LFLAGS} ${LIBS} -L/usr/local/lib -lm -lpthread -lrt

$(MPROGRAM): $(MOBJ)
	$(CC) $(CFLAGS) -o $(MPROGRAM) $(MOBJ) ${LFLAGS} ${MLIBS} ${LIBS} -L/usr/local/lib -lm -lpthread -lrt

PSLQ2.o: PSLQ2.cpp PSLQ2.h
	$(CC) $(CFLAGS) -c PSLQ2.cpp

pslqface.c : pslqface.tm
	${MPREP} -o pslqface.c pslqface.tm

pslqface.o : pslqface.c
	$(CC) ${CFLAGS} -c $<

dopslq.o: dopslq.cpp
	$(CC) ${CFLAGS} -c $<

clean:
	rm *.o -f
	rm pslqface -f
	rm pslqface.c -f
	rm dopslq -f

