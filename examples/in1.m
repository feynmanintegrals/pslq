SetOptions[$Output,PageWidth->Infinity];
Install["pslqface"];
x = -86.109767733362992037111085177361515372016704513500524047937870160995698410091970749366805819512161499658958394704000288456349460578145076;
list = {Zeta[4], Zeta[2]*Log[2]^2, Zeta[3]*Log[2], Log[2]^4,  Zeta[3], Zeta[2]*Log[2], Zeta[2], Log[2]^3,  Log[2]^2, Log[2], 1, PolyLog[4, 1/2]};
result = PSLQ[x,list,39,-34,0];
ToString[result, InputForm]
