## PSLQ

This is an algorithm to recover coefficients of some number decomposed as a linear combination of some basic constants.

This algorithm was originally implemented by Oleg Veretin and based on Parallel integer relation detection: Techniques and applications, David H. Bailey(LBL, Berkeley), David J. Broadhurst(Open U., England) (May, 1999) Published in: Math.Comput. 70 (2001) 1719-1736 • e-Print: math/9905048 [math.NA]

### Compilation

You should have Wolfram Mathematica and libcln-dev installed.

Now just run make

### Examples

The most basic examples can be called with `math < examples/in1.m` and similar

There is also a Mathematica notebook in the examples folder

An alternative way to use it without Mathematica is './dopslq < examples/pslq1.inp' and similar

