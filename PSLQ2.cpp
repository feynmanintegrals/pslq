/* 5 Nov 2006
 *   Oleg Veretin
 */

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "PSLQ2.h"

using namespace std;
using namespace cln;


/* modes */
#define MP 1
#define IP 2
#define DP 3


/* Result codes */
#define RESULT_CONTINUE            0
#define RESULT_RELATION_FOUND      1
#define RESULT_PRECISION_EXHAUSTED 2
#define RESULT_BOUND_NORM          3



/* correct this to change mode */
#define PSLQ_MODE DP




/* large and small bounds in DP mode */
#define DP_LARGE 1.0e+13
#define DP_SMALL 1.0e-13

/* number of decimal of precision in IP mode */
#define IP_NDP 26

/* PSLQ maximal bound norm */
#define PSLQ_BOUND          1.0e+20
#define PSLQ_ITERATIONS     1000000


/**************************************************************************
 *
 *  This is a 2-level PSLQ algorithm.
 *
 *  integer relation finding algorithm is decribed in e.g.                
 *  see                                                                 
 *          D.Bailey, M.Borwein and  R.Girgensohn                        
 *        "Experimental Evaluation of Euler Sums",                       
 *           RNR Technical Report RNR-93-014.                            
 *                                                                       
 *
 *  Here we can use PSLQ in 3 different modes: 
 *    MP -- multiprecision (this is original algorithm)
 *    IP -- using 2-level algo with intermediate precision numbers
 *    DP -- using 2-level algo with double precision
 *
 *   Mode is defined by preprocessor variable PSLQ_MODE to be set MP, IP or DP.
 *
 *  DP mode is the fastest (recomended), but by some large problems it may be
 *  not effective. Try then IP.
 *  MP mode is alwais safe but very slow.
 * 
 *                                                                      
 *  Usage:  res = PSLQ(xout, xin, pslq_ndp, pslq_nep, pslq_nep1)
 *
 *  arguments:                                                          
 *       xout        --- output vector of <cl_R>                             
 *       xin         --- input vector of <cl_R>                             
 *       pslq_ndp    --- nuber of decimals of precision  <long>          
 *       pslq_nep    --- log10 of detection threshold and/or <long>          
 *                  machine epsilon                                     
 *                  tipically take    nep = - ndp + 15
 *                                 or nep = - ndp + 25 for large problems
 *                                                                      
 *       pslq_nep1   --- log10 of confidence <long>
 *                       (this cuts "solutions" with large ymin/ymax)
 *               Actually the "solution" will be printed if ymin/ymax < 10^(pslq_nep1)
 *               Usually  pslq_nep1 = -15 is a good choice.
 *               To recover original PSLQ set pslq_nep to zero
 *                                                                      
 *  returns:                                                             
 *         -1     error                                                 
 *          1     relation detected                                     
 *                    (output in vector  xout)                          
 *          2     precision exhausted                                   
 *          3     bound norm exceeds    PSLQ_BOUND                      
 *          4     too many iterations   PSLQ_ITERATIONS                 
 *                                                                      
 **************************************************************************/

/* Note:
   we check the norm bound only in MP mode to save time
   */

/* here is the main function to call PSLQ2 */
int PSLQ2 (
        vector<cl_R>& xout,
        const vector<cl_R>  vec,
        const long pslq_ndp,
        const long pslq_nep,
        const long pslq_nep1
        );


// definition of a matrix A
#define c(A,i,j)  A[i+nn*(j-1)]


// hermitian reduction and update
#define REDUCE(imin,jmin,H,A,B,y)         \
    for ( i=imin; i<=nn; i++ ){             \
        for( j=jmin; j>0; j-- ){              \
            t = c(H,i,j)/c(H,j,j);              \
            t = my_round( t );                  \
            y[j] = y[j] + t*y[i];               \
            for ( k=1; k<=j; k++){              \
                c(H,i,k) = c(H,i,k) - t*c(H,j,k); \
            }                                   \
            for ( k=1; k<=nn; k++){             \
                c(A,i,k) = c(A,i,k) - t*c(A,j,k); \
                c(B,k,j) = c(B,k,j) + t*c(B,k,i); \
            }                                   \
        }                                     \
    }


/****  global variables *******/

long iteration;   // current iteration
long nn;          // length of input-vector

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////   helpful functions ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

inline cl_F my_round(cl_F x){ return fround(x); }
inline double my_round(double x){ return round(x); }

/* returns exponent of cl_F number x */
inline long  e10(const cl_F &x){
    return static_cast<long>(round(float_exponent(x)/3.321928095 - 1) ); }

    /* compute actual bound */
    inline void pslq_bound(cl_F H[], double &bound){ bound = 0;
        for ( int i=1; i<=nn-1; i++ )
            if ( abs( c(H,i,i) ) > bound ) bound = double_approx(abs( c(H,i,i)) );
        bound = 1/bound;
    }

/* set n X m matrix A to be of precision <decimals> */
void mat_set_prec(cl_F A[], long decimals, int n, int m){
    float_format_t ff = float_format(decimals);
    for (int i=1; i<=n; i++)
        for (int j=1; j<=m; j++) c(A,i,j) = cl_float(0, ff);
}

/* print matrix n X m */
void mat_print(cl_F A[], int n, int m){
    cout << "  Print matrix, float_format: " << float_format( c(A,1,1) ) << endl;
    for (int i=1; i<=n; i++){
        for (int j=1; j<=m; j++)  cout << setprecision(2) << double_approx( c(A,i,j) ) << "  ";
        cout << endl;
    }
}

void mat_print(double dA[], int n, int m){
    cout << "  Print matrix, DP" << endl;
    for (int i=1; i<=n; i++){
        for (int j=1; j<=m; j++)  cout <<  setprecision(2) << c(dA,i,j) << "  ";
        cout << endl;
    }
}

/* set matrix A to unit matrix */
inline void mat_unit(cl_F A[], int n, int m){
    float_format_t ff = float_format( c(A,1,1) );
    cl_F one = cl_float(1,ff);
    cl_F zero = cl_float(0,ff);
    for(int i=1; i<=n; i++) for(int j=1; j<=m; j++) c(A,i,j) = zero;
    for(int i=1; i<=min(n,m); i++) c(A,i,i) = one;
}

inline void DP_mat_unit(double dA[], int n, int m){
    int i,j;
    for(i=1; i<=n; i++) for(j=1; j<=m; j++) c(dA,i,j) = 0.0;
    for(i=1; i<=min(n,m); i++) c(dA,i,i) = 1.0;
}

/* copy matrix A to matrix B */
/* take accuracy of matrix B */
inline void mat_copy(cl_F A[], cl_F B[], int n, int m){
    float_format_t ff = float_format( c(B,1,1) );
    int i,j;
    for (i=1; i<=n; i++)
        for(j=1; j<=m; j++)
            c(B,i,j) = cl_float( c(A,i,j), ff);
}

inline void DP_mat_copy(double dA[], double dB[], int n, int m){
    int i,j;
    for (i=1; i<=n; i++) for(j=1; j<=m; j++) c(dB,i,j) = c(dA,i,j);
}


/* multiplication trom the right */
/* accuracy of A                 */
void mat_mul_right(cl_F A[], cl_F A1[], int n, int k, int m){
    float_format_t ff = float_format( c(A,1,1) );
    cl_F C[n*m+1];
    int i,j,r;

    for (i=1; i<=n; i++){
        for (j=1; j<=m; j++){
            c(C,i,j) = cl_float(0,ff);
            for (r=1; r<=k; r++)
                c(C,i,j) = c(C,i,j) + c(A,i,r) * cl_float( c(A1,r,j) ,ff);
        }
    }
    for (i=1; i<=n; i++) for (j=1; j<=m; j++) c(A,i,j) = c(C,i,j);
}


void DP_mat_mul_right(cl_F A[], double dA[], int n, int k, int m){
    float_format_t ff = float_format( c(A,1,1) );
    cl_F C[n*m+1];
    int i,j,r;

    for (i=1; i<=n; i++){
        for (j=1; j<=m; j++){
            c(C,i,j) = cl_float(0,ff);
            for (r=1; r<=k; r++)
                c(C,i,j) = c(C,i,j) + c(A,i,r) * cl_float( c(dA,r,j) ,ff);
        }
    }
    for (i=1; i<=n; i++) for (j=1; j<=m; j++) c(A,i,j) = c(C,i,j);
}


/* multiplication of vector by the square matrix trom the right */
/* accuracy of y                 */
void vec_mul_right(cl_F y[], cl_F A1[], int n){
    float_format_t ff = float_format( y[1] );
    cl_F C[n+1];
    int j,r;
    for (j=1; j<=n; j++){
        C[j] = cl_float(0,ff);
        for (r=1; r<=n; r++)
            C[j] = C[j] + y[r] * cl_float( c(A1,r,j) ,ff);
    }
    for (j=1; j<=n; j++) y[j] = C[j];
}

/* multiplication of MP vector by the square DP matrix trom the right */
/* accuracy of y                 */
void DP_vec_mul_right(cl_F y[], double dA[], int n){
    float_format_t ff = float_format( y[1] );
    cl_F C[n+1];
    int j,r;
    for (j=1; j<=n; j++){
        C[j] = cl_float(0,ff);
        for (r=1; r<=n; r++)
            C[j] = C[j] + y[r] * cl_float( c(dA,r,j) ,ff);
    }
    for (j=1; j<=n; j++) y[j] = C[j];
}


/* multiplication trom the left */
/* accuracy of A                 */
void mat_mul_left(cl_F A1[], cl_F A[], int n, int k, int m){
    float_format_t ff = float_format( c(A,1,1) );
    cl_F C[n*m+1];
    int i,j,r;

    for (i=1; i<=n; i++){
        for (j=1; j<=m; j++){
            c(C,i,j) = cl_float(0,ff);
            for (r=1; r<=k; r++)
                c(C,i,j) = c(C,i,j) + cl_float( c(A1,i,r) ,ff) * c(A,r,j);
        }
    }
    for (i=1; i<=n; i++) for (j=1; j<=m; j++) c(A,i,j) = c(C,i,j);
}


void DP_mat_mul_left(double dA[], cl_F A[], int n, int k, int m){
    float_format_t ff = float_format( c(A,1,1) );
    cl_F C[n*m+1];
    int i,j,r;

    for (i=1; i<=n; i++){
        for (j=1; j<=m; j++){
            c(C,i,j) = cl_float(0,ff);
            for (r=1; r<=k; r++)
                c(C,i,j) = c(C,i,j) + cl_float( c(dA,i,r) ,ff) * c(A,r,j);
        }
    }
    for (i=1; i<=n; i++) for (j=1; j<=m; j++) c(A,i,j) = c(C,i,j);
}

//////////////////////////////////////////////////////////////////////////////

/* in this function accuracy is determined from the y[1] */
void init_pslq(cl_F y[], cl_F H[], cl_F A[], cl_F B[])
{
    float_format_t ff = float_format( y[1] );
    cl_F one = cl_float(1,ff);
    cl_F zero = cl_float(0,ff);
    long i, j;
    cl_F t = cl_float(0,ff), u = cl_float(0,ff);
    cl_F s[nn+1];

    /* Compute partial sums. */
    for (i = nn; i >= 1; i--) {
        t = t + y[i]*y[i];
        s[i] = sqrt(t);
    }
    t = one/s[1];

    /* Normalize vector y and put it into y.  Normalize s as well. */
    for (i = 1; i <= nn; i++) {
        y[i] = y[i] * t;
        s[i] = s[i] * t;
    }

    /* Set matrix A, B to the identity. */
    mat_unit(A, nn,nn);
    mat_unit(B, nn,nn);

    /* compute initial matrix H */
    /* Set H matrix to lower trapezoidal basis of perp(x). */

    for (i=1; i<=nn; i++) for (j=1; j<=nn-1; j++ ) c(H,i,j) = zero;

    for (i=1; i<=nn; i++ ){
        if ( i <= nn-1){
            c(H,i,i) = s[i+1]/s[i];
        }
        for ( j=1; j<i; j++ ){
            c(H,i,j) = - y[i]*y[j]/(s[j]*s[j+1]);
        }
    }
}

/////////////////// INIT 2

int init_pslq2(cl_F y[], cl_F H[], cl_F A[], cl_F B[], cl_F y1[], cl_F H1[], cl_F A1[], cl_F B1[])
{
    float_format_t ff  = float_format(y[1]);
    float_format_t ff1 = float_format(y1[1]);

    int i, j;
    int result = RESULT_CONTINUE;

    cl_F eps1 = cl_float( expt( cl_float(10,ff1), -IP_NDP+5 ), ff1 );  // same as IP_SMALL
    cl_F u, v, t;

    /* Find the min and max magnitude in the y vector.  If the dynamic
       range of the y vector is too large, abort the initialization.    */

    u = cl_float(0,ff);
    v = cl_float(1.0e+30,ff);
    for ( i=1; i<=nn; i++){
        t = abs(y[i]);
        if ( u < t ) u = t;
        if ( v > t ) v = t;
    }

    t = v / u;

    if (t < eps1) {
        result = RESULT_PRECISION_EXHAUSTED;
    } else {

        /* Set y1 to be the scaled y vector. */
        t = cl_float(1,ff) / u;
        for (i = 1; i<=nn; i++) y1[i] = cl_float( y[i] * t, ff1 );
    }

    /* Find the max and min magnitude along diagonal of H. */
    u = cl_float(0,ff);
    v = cl_float(1.0e+30,ff);
    for (i=1; i<=nn; i++){
        t = abs( c(H,i,i) );
        if ( u < t ) u = t;
        if ( v > t ) v = t;
    }

    if ( v/u < eps1 ){
        result = RESULT_PRECISION_EXHAUSTED;
    } else{
        /* Set H1 to be the scaled H matrix. */
        t = cl_float(1,ff) / u;
        for (j = 1; j <= nn-1; j++) {
            for (i = 1; i <= nn; i++) {
                c(H1,i,j) = cl_float( c(H,i,j) * t, ff1 );
            }
        }

        /* Set da and db to the identity. */
        mat_unit(A1,nn,nn);
        mat_unit(B1,nn,nn);
    }

    return result;
}


/////////////////// INIT 3

int DP_init_pslq2(cl_F y[], cl_F H[], cl_F A[], cl_F B[], double dy[], double dH[], double dA[], double dB[])
{
    int i, j;
    int result = RESULT_CONTINUE;

    double eps1 = DP_SMALL;
    float_format_t ff = float_format( y[1] );
    cl_F u, v, t;

    /* Find the min and max magnitude in the y vector.  If the dynamic
       range of the y vector is too large, abort the initialization.    */

    u = cl_float(0,ff);
    v = cl_float(1.0e+30,ff);
    for (i=1; i<=nn; i++){
        t = abs(y[i]);
        if ( u < t ) u = t;
        if ( v > t ) v = t;
    }

    t = v / u;

    if (t < eps1) {
        result = RESULT_PRECISION_EXHAUSTED;
        cout << t << endl;
    } else {

        /* Set y1 to be the scaled y vector. */
        t = cl_float(1,ff) / u;
        for (i = 1; i<=nn; i++) dy[i] = double_approx( y[i] * t );
    }

    /* Find the max and min magnitude along diagonal of H. */
    u = cl_float(0,ff);
    v = cl_float(1.0e+30,ff);
    for (i=1; i<=nn; i++){
        t = abs( c(H,i,i) );
        if ( u < t ) u = t;
        if ( v > t ) v = t;
    }

    if ( !1 ){
        result = RESULT_PRECISION_EXHAUSTED;
        cout << "second\n";
        //    mat_print(H,nn,nn-1);
        cout << double_approx(v) << endl;
        cout << double_approx(u) << endl;
    } else{
        /* Set dH to be the scaled H matrix. */
        t = cl_float(1,ff) / u;
        for (j = 1; j <= nn-1; j++) {
            for (i = 1; i <= nn; i++) {
                c(dH,i,j) = double_approx( c(H,i,j) * t );
            }
        }

        /* Set da and db to the identity. */
        DP_mat_unit(dA,nn,nn);
        DP_mat_unit(dB,nn,nn);
    }

    return result;
}


//////////////////////////////////////////////////////////////////////////////////////

void iterate_pslq ( cl_F H[], cl_F A[], cl_F B[], cl_F y[] )
{
    float_format_t ff = float_format( y[1] );
    cl_F one = cl_float(1,ff);
    cl_F zero = cl_float(0,ff);

    long m;
    long i, j, k;
    double gami, maxHii, doub, gam = 1.154700538;

    cl_F t  = cl_float(0,ff);
    cl_F t0 = cl_float(0,ff);
    cl_F t1 = cl_float(0,ff);
    cl_F t2 = cl_float(0,ff);
    cl_F t3 = cl_float(0,ff);
    cl_F t4 = cl_float(0,ff);

    /* find m such that gam^i*abs(H[i,i]) is maximal */

    m = 1; gami = 1.0; maxHii = 0.0;
    for ( i=1; i<nn; i++){
        gami = gami*gam;
        doub = gami*fabs( double_approx( c(H,i,i) ) );
        if ( doub > maxHii ){
            maxHii = doub;
            m = i;
        }
    }

    /*  block reduction on H updating y, A, B */

    REDUCE(m+1, min(i-1,m+1), H, A, B, y);

    /*  exchange  */

    swap(y[m],y[m+1]);

    for ( j=1; j<=nn; j++ ){
        if ( j<nn ) swap( c(H,m,j), c(H,m+1,j) );
        swap( c(A,m,j), c(A,m+1,j) );
        swap( c(B,j,m), c(B,j,m+1) );
    }

    /*  update matrix H */

    if ( m <= nn-2 ){
        t0 = sqrt( c(H,m,m)*c(H,m,m) + c(H,m,m+1)*c(H,m,m+1) );
        t1 = c(H,m,m)/t0;
        t2 = c(H,m,m+1)/t0;
        for ( i=m; i<=nn; i++ ){
            t3 = c(H,i,m);
            t4 = c(H,i,m+1);
            c(H,i,m) = t1*t3 + t2*t4;
            c(H,i,m+1) = - t2*t3 + t1*t4;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////

void DP_iterate_pslq ( double dH[], double dA[], double dB[], double dy[] )
{
    long m, i, j, k;
    double t, t0, t1, t2, t3, t4, gami, maxHii, doub, gam = 1.154700538;

    /* find m such that gam^i*abs(dH[i,i]) is maximal */

    m = 1; gami = 1.0; maxHii = 0.0;
    for ( i=1; i<nn; i++){
        gami = gami*gam;

        //QUQUQUQU
        doub = gami*fabs( c(dH,i,i) );
        if ( doub > maxHii ){
            maxHii = doub;
            m = i;
        }
    }

    /*  block reduction on dH updating dy, dA, dB */

    REDUCE(m+1, min(i-1,m+1), dH, dA, dB, dy);

    /*  exchange  */

    swap(dy[m],dy[m+1]);

    for ( j=1; j<=nn; j++ ){
        if ( j<nn ) swap( c(dH,m,j), c(dH,m+1,j) );
        swap( c(dA,m,j), c(dA,m+1,j) );
        swap( c(dB,j,m), c(dB,j,m+1) );
    }

    /*  update matrix dH */

    if ( m <= nn-2 ){
        t0 = sqrt( c(dH,m,m)*c(dH,m,m) + c(dH,m,m+1)*c(dH,m,m+1) );
        t1 = c(dH,m,m)/t0;
        t2 = c(dH,m,m+1)/t0;
        for ( i=m; i<=nn; i++ ){
            t3 = c(dH,i,m);
            t4 = c(dH,i,m+1);
            c(dH,i,m) = t1*t3 + t2*t4;
            c(dH,i,m+1) = - t2*t3 + t1*t4;
        }
    }
}


int check_status(cl_F A[], cl_F y[], const cl_F teps, const cl_F Amax_admitted,
        cl_F & ymin, cl_F & ymax, cl_F & Amax)

{
    float_format_t ff = float_format( y[1] );

    long i;
    int result = RESULT_CONTINUE;

    cl_F t0  = cl_float(0,ff);

    /*                                             */
    /*  calculate ymin, ymax, Amax                 */
    /*     and m, correspondig to smallest y       */

    Amax = cl_float(0,ff);
    ymax = cl_float(0,ff);
    ymin = cl_float(1.0e+30,ff);
    for ( i=1; i<=nn; i++){
        t0 = abs(y[i]);
        if ( ymax < t0 ) ymax = t0;
        if ( ymin > t0 ) ymin = t0;
    }
    for ( i=1; i<=nn*nn; i++){
        t0 = abs(A[i]);
        if ( Amax < t0 ) Amax = t0;
    }

    if ( ymin < teps ){
        result = RESULT_RELATION_FOUND;
    }
    if (Amax > Amax_admitted) {
        result = RESULT_PRECISION_EXHAUSTED;
    }

    return result;
}


int DP_check_status(double dA[], double dy[], const double dteps, const double dAmax_admitted,
        double & dymin, double & dymax, double & dAmax)

{
    long i;
    int result = RESULT_CONTINUE;
    double t0;

    /*                                             */
    /*  calculate ymin, ymax, Amax                 */
    /*     and m, correspondig to smallest y       */

    dAmax = 0.0;
    dymax = 0.0;
    dymin = 1.0e+30;
    for (i=1; i<=nn; i++){
        t0 = fabs(dy[i]);
        if ( dymax < t0 ) dymax = t0;
        if ( dymin > t0 ) dymin = t0;
    }
    for (i=1; i<=nn*nn; i++){
        t0 = fabs(dA[i]);
        if ( dAmax < t0 ) dAmax = t0;
    }

    if ( dymin < dteps ){
        result = RESULT_RELATION_FOUND;
    }
    if (dAmax > dAmax_admitted) {
        result = RESULT_PRECISION_EXHAUSTED;
    }

    return result;
}


////////////////////////////////////////////////////////////////////////////////

/* Computes a LQ decomposition of the matrix a, and puts the lower
 *   triangular matrix L into a.
 */

void lq_decomposition(cl_F A[], int n, int m)
{
    int i, j, k;
    int min_mn = min(m, n);
    float_format_t ff = float_format( c(A,1,1) );
    // chto s tochnost'yu???
    cl_F zero = cl_float(0,ff), one = cl_float(1,ff), two = cl_float(2,ff);
    cl_F  t = zero, x1 = zero, norm2u = zero;
    cl_F  u[m+1];

    for(i = 1; i < min_mn; i++) {

        /* Compute the Householder vector. */

        x1 = c(A,i,i);

        /* compute the norm of vector x1 (the i-th row) */
        t = zero;
        for (j = i; j <= m; j++) {
            t = t + c(A,i,j)*c(A,i,j);
        }
        // here nicht vergessen!
        //    if (t == 0) continue;

        norm2u = t;
        t = sqrt(t);
        norm2u = two*norm2u + two*x1*t*float_sign(x1);
        x1 = x1 + float_sign(x1)*t;
        u[i] = x1;
        for ( j=i+1; j<=m; j++) u[j] = c(A,i,j);
        norm2u = two/norm2u;

        /* transform now rows */

        for ( k = i; k <= n; k++){
            /* compute scalar product of row[k] and vector u */
            t = zero;
            for ( j = i; j <=m; j++){
                t = t + u[j]*c(A,k,j);
            }
            t = norm2u*t;

            c(A,k,i) = c(A,k,i) - t*x1;
            for( j = i+1; j <= m; j++){
                c(A,k,j) = c(A,k,j) - t*u[j];
            }
        }
    }

    /* Set the upper half of a to zero. */
    for (j = 1; j <= m; j++) for (i = 1; i < j; i++) c(A,i,j) = zero;

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////   main function    ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

int PSLQ2 (
        vector<cl_R> & xout,
        const vector<cl_R>  &vec,
        const long pslq_ndp,
        const long pslq_nep,
        const long pslq_nep1
        )
{

    nn = vec.size();              //  length of the input vector
    float_format_t f = float_format(pslq_ndp);
    cl_F epsilon = float_epsilon(f);

    double bound;   // bound norm
    long i, j, k, m, mode;
    int result;

    cl_F_div_t R;

    /* Here are large and small number in different modes. */
    /* Note that in DP mode we use DP_LARGE and DP_SMALL.  */
    cl_F MP_LARGE = cl_float( expt( cl_float(10,f), pslq_ndp ), f ); //  this is   10^(pslq_ndp)
    cl_F MP_SMALL = cl_float( expt( cl_float(10,f), pslq_nep ), f );  //  this is  10^(pslq_nep)
    cl_F Enep1 = cl_float( expt( cl_float(10,f), pslq_nep1 ),f );     //  this is  10^nep1

    cl_F IP_LARGE = cl_float( expt( cl_float(10,f), IP_NDP-5 ), f );    //  this is  10^ndp
    cl_F IP_SMALL = cl_float( expt( cl_float(10,f), -IP_NDP+5 ), f );    //  this is  10^nep

    if ( MP_LARGE * MP_SMALL < 1.0e5 ){
        cout << "PSLQ2: Check parameters 'pslq_ndp' and 'pslq_nep'!" << endl;
        return -1;
    }

    /* here are working storage arrays */
    /***** high precision numbers *****/
    cl_F t, ymax, ymin, Amax;
    cl_F y[nn+1], A[nn*nn+1], B[nn*nn+1], H[nn*(nn-1)+1];

    /***** low precision numbers *****/
    cl_F y1[nn+1], A1[nn*nn+1], B1[nn*nn+1], H1[nn*(nn-1)+1];
    cl_F A1sav[nn*nn+1], B1sav[nn*nn+1];

    /***** double precision numbers *****/
    double dymax, dymin, dAmax;
    double dy[nn+1], dA[nn*nn+1], dB[nn*nn+1], dH[nn*(nn-1)+1];
    double dAsav[nn*nn+1], dBsav[nn*nn+1];

    mat_set_prec(y,pslq_ndp, nn,1);
    mat_set_prec(A,pslq_ndp, nn,nn);
    mat_set_prec(B,pslq_ndp, nn,nn);
    mat_set_prec(H,pslq_ndp, nn,nn-1);

    mat_set_prec(y1,IP_NDP, nn,1);
    mat_set_prec(A1,IP_NDP, nn,nn);
    mat_set_prec(B1,IP_NDP, nn,nn);
    mat_set_prec(H1,IP_NDP, nn,nn-1);

    mat_set_prec(A1sav,IP_NDP, nn,nn);
    mat_set_prec(B1sav,IP_NDP, nn,nn);

    cout << endl;

    /*                      */
    /*  read the PSLQ data  */
    /*                      */

    cout << "PSLQ data:" << endl;
    for ( i = 1; i<=nn; i++ ){
        y[i] = cl_float( vec[i-1], f );
        cout << "x" << i << "= " << double_approx(y[i]) << endl;
    }

    cout << endl;
    cout << "------PSLQ test-------" << endl;

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    /* MP initialization */
    init_pslq(y, H, A, B);
    REDUCE(2, i-1, H, A, B, y);
    lq_decomposition(H, nn, nn-1);

    /*********************************************************************************/
    /*     repeat now              */
    /*********************************************************************************/

    result = RESULT_CONTINUE;
    mode = MP;
    iteration = 0;
    cout << "MODE MP,  it: " << iteration + 1 << endl;

    /*****************  begin of MP cycle ************************/
    while ( ( result == RESULT_CONTINUE ) && (mode == MP) ){   // MP while


        /*********************************************/
        /* this piece starts the DP level of PSLQ    */
        /*********************************************/

#if PSLQ_MODE == DP

        /* try to initialize DP mode */

reinitDP:
        lq_decomposition(H,nn,nn-1);
        result = DP_init_pslq2(y,H,A,B, dy,dH,dA,dB);

        // initialization fails
        if ( result == RESULT_PRECISION_EXHAUSTED ){
            result = RESULT_CONTINUE;
            mode = MP;
            cout << "initDP: initialization fails." << endl;
            goto continueMP;
        }

        if ( result == RESULT_CONTINUE ){
            mode = DP;
            cout << "-->MODE DP,  it: " << iteration + 1<< endl;
        }

        // if initialisation is ok 
        /*****************  begin of DP cycle ************************/
        while ( ( result == RESULT_CONTINUE ) && (mode == DP) ){   // DP while

            /* perform one step DP iteration */
            if ( ( result == RESULT_CONTINUE) && (mode == DP) ){
                //cout << "-->MODE DP, it: " << iteration + 1<< endl;
                DP_mat_copy(dA,dAsav,nn,nn);
                DP_mat_copy(dB,dBsav,nn,nn);
                DP_iterate_pslq(dH, dA, dB, dy);
                result = DP_check_status(dA, dy, DP_SMALL, DP_LARGE, dymin, dymax, dAmax);
                iteration++;
            }

        }  /*****************  end of DP cycle ************************/


        if ( result == RESULT_PRECISION_EXHAUSTED ){
            DP_mat_copy(dAsav,dA,nn,nn);
            DP_mat_copy(dBsav,dB,nn,nn);
            iteration--;
        }

        /* update MP arrays and check for status */
        DP_vec_mul_right(y,dB,nn);
        DP_mat_mul_right(B,dB,nn,nn,nn);
        DP_mat_mul_left(dA,A,nn,nn,nn);
        DP_mat_mul_left(dA,H,nn,nn,nn-1);
        result = check_status(A, y, MP_SMALL, MP_LARGE, ymin, ymax, Amax);
        pslq_bound(H, bound);
        if ( bound > PSLQ_BOUND ) result = RESULT_BOUND_NORM;

        switch ( result ){
            case RESULT_CONTINUE:
                // mode stays DP
                goto reinitDP;
                break;
            case RESULT_RELATION_FOUND:
                mode = MP;
                break;
            case RESULT_PRECISION_EXHAUSTED:
                mode = MP;
                break;
            case RESULT_BOUND_NORM:
                mode = MP;
                break;
            default:
                cout << "PSLQ2: should not be here." << endl;
                exit(0);
        }

#endif   /* the end of the DP level of PSLQ */


        /*********************************************/
        /* this piece starts the 2nd level of PSLQ   */
        /*********************************************/

#if PSLQ_MODE == IP

        /* try to initialize IP mode */

reinit2:
        lq_decomposition(H,nn,nn-1);
        result = init_pslq2(y,H,A,B, y1,H1,A1,B1);

        // initialization fails
        if ( result == RESULT_PRECISION_EXHAUSTED ){
            result = RESULT_CONTINUE;
            mode = MP;
            cout << "init2: initialization fails." << endl;
            goto continueMP;
        }

        if ( result == RESULT_CONTINUE ){
            mode = IP;
            cout << "-->MODE IP,  it: " << iteration + 1<< endl;
        }

        // if initialisation is ok
        /*****************  begin of IP cycle ************************/
        while ( ( result == RESULT_CONTINUE ) && (mode == IP) ){   // IP while

            /* perform one step IP iteration */
            if ( ( result == RESULT_CONTINUE) && (mode == IP) ){
                //cout << "-->MODE IP, it: " << iteration + 1<< endl;
                mat_copy(A1,A1sav,nn,nn);
                mat_copy(B1,B1sav,nn,nn);
                iterate_pslq(H1, A1, B1, y1);
                result = check_status(A1, y1, IP_SMALL, IP_LARGE, ymin, ymax, Amax);
                iteration++;
            }

        }  /*****************  end of IP cycle ************************/

        if ( result == RESULT_PRECISION_EXHAUSTED ){
            mat_copy(A1sav,A1,nn,nn);
            mat_copy(B1sav,B1,nn,nn);
            iteration--;
        }

        /* update MP arrays and check for status */
        vec_mul_right(y,B1,nn);
        mat_mul_right(B,B1,nn,nn,nn);
        mat_mul_left(A1,A,nn,nn,nn);
        mat_mul_left(A1,H,nn,nn,nn-1);
        result = check_status(A, y, MP_SMALL, MP_LARGE, ymin, ymax, Amax);
        pslq_bound(H, bound);
        if ( bound > PSLQ_BOUND ) result = RESULT_BOUND_NORM;

        switch ( result ){
            case RESULT_CONTINUE:
                // mode stays IP
                goto reinit2;
                break;
            case RESULT_RELATION_FOUND:
                mode = MP;
                break;
            case RESULT_PRECISION_EXHAUSTED:
                mode = MP;
                break;
            case RESULT_BOUND_NORM:
                mode = MP;
                break;
            default:
                cout << "PSLQ2: should not be here." << endl;
                exit(0);
        }

#endif   /* the end of the 2nd level of PSLQ */

continueMP:

        /* perform one step MP iteration */
        if ( ( result == RESULT_CONTINUE) && (mode == MP) ){
            // cout << "MODE MP, it: " << iteration + 1<< endl;
            iterate_pslq(H, A, B, y);
            result = check_status(A, y, MP_SMALL, MP_LARGE, ymin, ymax, Amax);
            iteration++;
        }

        /* check for status */

        /* additional check */
        if ( ( result == RESULT_RELATION_FOUND) && ( ymin/ymax > Enep1 ) ){
            result = RESULT_CONTINUE;
        }

        /* compute actual bound */
        pslq_bound(H, bound);
        if ( bound > PSLQ_BOUND ) {
            result = RESULT_BOUND_NORM;
        }

    }  /*****************  end of MP cycle ************************/

    /*               */
    /*  finish PSLQ  */
    /*               */

    pslq_bound(H,bound);

    cout << "PSLQ stop: at iteration " << iteration << endl;
    cout << "\n  decimals: of accuracy:   " << pslq_ndp
        << "\n  detection threshold:     " << pslq_nep
        << "\n  add. thresh. (nep1):    " << pslq_nep1 << endl << endl;
    cout << "   lg(ymin) = " << e10(ymin) << endl;
    cout << "   lg(ymax) = " << e10(ymax) << endl;
    cout << "   norm = " << bound << endl << endl;

    if ( result == RESULT_PRECISION_EXHAUSTED ){
        cout << "Pecision exhausted." << endl;
        cout << "Amax = " << Amax << endl;
        //    mat_print(A,nn,nn);
    }

    if ( result == RESULT_BOUND_NORM ){
        cout << "Bound norm exceeds "  << PSLQ_BOUND << endl;
    }

    if ( result == RESULT_RELATION_FOUND ){
        cout << "Relation found." << endl << endl;

        /* find corresponding m */
        m = 1;
        t = cl_float(1.0e+100,f);
        for ( i=1; i<=nn; i++ ){
            if ( abs(y[i]) < t ){
                t = abs(y[i]);
                m = i;
            }
        }

        //    for ( i=1; i<=nn; i++ ) { t = fround( c(B,i,m) ); }
        // check that all is correct:
        // Dont forget that there is a shift by 1 of indices
        // of y-vector and vec-vector
        t = cl_float(0,f);
        for ( i=1; i<=nn; i++ ) { t = t + c(B,i,m)*vec[i-1]; }
        cout << "with\n    |c1*x1+...+cn*xn| < " << "10^" << e10(t)+1 << endl;
        cout << "with\n    confidence  " << e10(ymin/ymax) << endl << endl;

        for ( i=1; i<=nn; i++ ) {
            R = round2( c(B,i,m) );
            cout << "c" << i << ": " << R.quotient << endl;
            xout.push_back( R.quotient );
        }
        cout << endl;
    }

    cout << endl;

    return result;
}  /* end of PSLQ2 */


